<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Redirect / to /home
Route::get('/', function() {
    return redirect('/home');
});

// -- Public routes --

// Homepage
Route::get('/home', 'HomeController@home');

// Search

// Language

//Notification
Route::get('/notification', 'NotificationPageController@show');
Route::delete('/notification/{not}', 'NotificationPageController@delete')->middleware('auth');
Route::put('/notification/{not}', 'API\NotificationChange')->middleware('auth');

// About
Route::get('/about', 'AboutUsController@show');

// MyTicket
Route::get('/my-tickets', 'MyTicketsController@show');

// Registration
Route::post('/register', 'Auth\RegisterController@createUser');

// Login
Route::post('/login', 'Auth\LoginController@login');

// Reset password
Route::get('/reset-password', 'Auth\ResetPasswordController@reset');

// Search Page
Route::get('/search', 'SearchController@show');
Route::post('/search', 'SearchController@search');

// Event Detail
Route::get('/event/{event}', 'EventController@showDetail');

//Event Add to Cart
Route::post('/addtocart', 'EventController@addItemsToCart')->middleware('auth');

//Order Successfull
Route::post('/buy-items','ShoppingCartController@buyItems')->middleware('auth');

// -- protected routes --

// Logout
Route::get('/logout', 'Auth\LoginController@logout')->middleware('auth');

// Account
Route::get('/account', 'AccountController@show')->middleware('auth');

//Shopping Cart
Route::get('/cart', 'ShoppingCartController@show')->middleware('auth');
Route::delete('/cart/delete/{id}', 'ShoppingCartController@remove')->middleware('auth');

//Ordered Items
Route::get('/ordered', 'OrderedController@show')->middleware('auth');

// Admin Console
Route::get('/admin', 'AdminController@show')->middleware('auth');
Route::get('/admin/table/{tableName}', 'AdminController@fetchTable')->middleware('auth');
Route::get('/admin/table/{tableName}/{id}', 'AdminController@fetchRecordFromTable')->middleware('auth');
Route::delete('/admin/table/{tableName}/{id}', 'AdminController@deleteRecordFromTable')->middleware('auth');
Route::put('/admin/table/{tableName}', 'AdminController@saveRecordInTable')->middleware('auth');

// Vendor
Route::get('/vendor/manage/menu', 'VendorController@showDash')->middleware('auth');
    // Create event
Route::get('/vendor/manage/create', 'VendorController@showCreate')->middleware('auth');
Route::post('/vendor/manage/create', 'VendorController@createEvent')->middleware('auth');
Route::put('/vendor/manage/create/venue', 'VendorController@createVenue')->middleware('auth');
    // Manage Events
Route::get('/vendor/manage/events', 'VendorController@showEvents')->middleware('auth');
Route::get('/vendor/manage/events/list/{id}', 'VendorController@retrieveEvents')->middleware('auth');
Route::get('/vendor/manage/events/{id}', 'VendorController@showEventDetail')->middleware('auth');
Route::delete('/vendor/manage/tickets/{id}', 'VendorController@deleteTicket')->middleware('auth');
Route::delete('/vendor/manage/events/{id}', 'VendorController@deleteEvent')->middleware('auth');

// -- API --

Route::put('/api/search/event', 'AutosuggestController@searchEvent');

// -- API (protected) --

// Autosuggest
Route::put('/api/search/venue', 'AutosuggestController@searchVenue')->middleware('auth');
Route::put('/api/search/city', 'AutosuggestController@searchCity')->middleware('auth');
Route::put('/api/search/artist', 'AutosuggestController@searchArtist')->middleware('auth');
