<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyTicketsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        return view('tickets.mytickets', ['tickets' => auth()->user()->boughtTickets()->get()]);
    }
}
