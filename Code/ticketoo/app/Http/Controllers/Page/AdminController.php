<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Hash;

use App\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Authentication Middleware
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
        $user = Auth::user();
        if($user->type == 'ADMIN') {
            return view('admin.admin');
        } else {
            return redirect('home/');
        }
        
    }

    public function fetchTable($tableName)
    {
        $user = Auth::user();
        if($user->type == 'ADMIN') {
            $records = DB::table($tableName)->get();
            return view('includes.any.' . $tableName . '-table', ['records' => $records]);
        } else {
            return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
        }
    }


    public function fetchRecordFromTable($tableName, $id)
    {
        $user = Auth::user();
        if($user->type == 'ADMIN') {
            $record = DB::table($tableName)->where('id', $id)->first();
            return  response()->json([
                'record' => $record,
            ]);
        } else {
            return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
        }
    }

    public function deleteRecordFromTable($tableName, $id) {
        $user = Auth::user();
        if($user->type == 'ADMIN') {
            DB::table($tableName)->where('id', $id)->delete();
        } else {
            return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
        }
    }

    public function saveRecordInTable(string $tableName, Request $request) {
        $user = Auth::user();
        if($user->type == 'ADMIN') {
            switch($tableName) {
                case 'user': $this->saveUser($request); break;
            }
        } else {
            return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
        }
    }

    private function saveUser(Request $request) {
        if($request->input('id') != null) {
            $record = User::find($request->input('id'));
        } else {
            $record = new User;
        }
        if($request->input('email') != null) {
            $record->email = $request->input('email');
        }
        if($request->input('password') != null) {
            $record->password = Hash::make($request->input('password'));
        }
        if($request->input('name') != null) {
            $record->name = $request->input('name');
        }
        if($request->input('type') != null) {
            $record->type = $request->input('type');
        }
        $record->save();
    }

}
