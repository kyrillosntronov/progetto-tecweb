<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Event;

use App\Category;

use App\Participation;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
        $userName = 'Guest';
        if (Auth::check()) {
            $userName = Auth::user()->name;
        }
        $categories = Category::all();
        $emptyEvents = [];
        return view('search.search-page', ['userName' => $userName, 'categories' => $categories, 'searchResults' => $emptyEvents, 'isShown' => true]);
    }

    public function search(Request $request) {
        $category = $request->input('category');
        $venue = $request->input('venue');
        $city = $request->input('city');
        $dateFrom = $request->input('dateFrom');
        $dateTo = $request->input('dateTo');
        $artist = $request->input('artist');
        $name = $request->input('name');

        $query = Participation::select('event.id as id', 'category.name as category', 'event.name as name', 'venue.name as venueName', 'city.name as cityName', 'event.date as date')
            ->join('event', 'participation.id_event', '=', 'event.id')
            ->join('artist', 'participation.id_artist', '=', 'artist.id')
            ->join('venue', 'event.id_venue', '=', 'venue.id')
            ->join('city', 'venue.id_city', '=', 'city.id')
            ->join('category', 'event.id_category', '=', 'category.id');
        
        if($category && $category > 0) $query->where('event.id_category', '=', $category);

        if($venue) $query->where('venue.name', 'like', '%' . $venue . '%');

        if($city) $query->where('city.name', 'like', '%' . $city . '%');

        if($artist) $query->where('artist.name', 'like', '%' . $artist . '%');

        if($name) $query->where('event.name', 'like', '%' . $name . '%');

        if($dateFrom && $dateTo) $query->whereBetween('event.date', [$dateFrom, $dateTo]);
        
        $events = $query->orderBy('event.date')->take('10')->get();

        $filteredEvents = [];
        foreach($events as $event) {
            $isContained = false;
            foreach($filteredEvents as $filtered) {
                if($filtered == $event) {
                    $isContained = true;
                }
            }
            if(!$isContained) {
                array_push($filteredEvents, $event);
            }
        }

        $userName = 'Guest';
        if (Auth::check()) {
            $userName = Auth::user()->name;
        }
        $categories = Category::all();
        return view('search.search-page', ['userName' => $userName, 'categories' => $categories, 'searchResults' => $filteredEvents, 'isShown' => false]);
    }
}
