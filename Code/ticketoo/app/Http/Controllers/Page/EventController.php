<?php

namespace App\Http\Controllers;

use App\Notification;
use App\ShoppingCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Event;
use App\Participation;

class EventController extends Controller
{

    public function __construct()
    {
    }

    public function showDetail(Request $request, Event $event)
    {
        $userName = 'Guest';
        if (Auth::check()) {
            $userName = Auth::user()->name;
        }
        $avail = $event->tickets()->where('is_locked', 0)->count();
        $booked = $event->tickets()->where('is_locked',1)->count();
        $sold = $event->tickets()->where('sold', true)->count();

        // Ticket groups
        $ticketGroups = $event->tickets()->select('sector', 'price')->groupBy('sector', 'price')->get();

        // Artists
        $artists = Participation::select('artist.name as artist')
        ->join('artist', 'artist.id', '=', 'participation.id_artist')
        ->join('event', 'event.id', '=', 'participation.id_event')
        ->where('participation.id_event', '=', $event->id)
        ->get();

        return view('event.event', [
            'userName' => $userName, 'event' => $event, 'available' => $avail, 'sold' => $sold, 'booked' => $booked,
            'ticketGroups' => $ticketGroups, 'artists' => $artists
        ]);
    }

    public function addItemsToCart(Request $request){
        $eventID = $request->input('event');
        $sector = $request->input('sector');
        $quantity = $request->input('quantity');
        $event = Event::find($eventID);
        $tickets = $event->tickets()->where('is_locked', false)
                                    ->where('sector', $sector)
                                    ->limit($quantity) ->get();
        foreach($tickets as $ticket) {
            $item = new ShoppingCart();
            $item->ticket_id = $ticket->id;
            $item->user_id = Auth::id();
            $item->save();
            $ticket->is_locked = true;
            $ticket->save();
        }

        return redirect('/event/'.$eventID);
    }

}
