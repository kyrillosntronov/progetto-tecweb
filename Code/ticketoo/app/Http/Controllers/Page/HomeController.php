<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Event;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        $events = Event::select('event.id as id', 'event.name as name', 'event.date as date', 'venue.name as venue')
            ->join('venue', 'event.id_venue', '=', 'venue.id')
            ->orderBy('date')
            ->take('4')
            ->get();
        $userName = 'Guest';
        if (Auth::check()) {
            $userName = Auth::user()->name;
        }
        return view('homepage.home', ['userName' => $userName, 'events' => $events]);
    }
}
