<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

use App\Category;

use App\Venue;

use App\Event;

use App\Participation;

use App\Artist;

use App\Ticket;

class VendorController extends Controller
{
    /**
     * Show the vendor dashboard
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showDash()
    {
        $user = Auth::user();
        if($user->type == 'VENDOR') {
            return view('vendor.vendor-dash');
        } else {
            return redirect('home/');
        }
        
    }

    public function showCreate()
    {
        $user = Auth::user();
        if($user->type == 'VENDOR') {
            $categories = Category::all();
            return view('vendor.vendor-create', ['categories' => $categories, 'vendor_id' => $user->id]);
        } else {
            return redirect('home/');
        }
        
    }

    public function showEvents()
    {
        $user = Auth::user();
        if($user->type == 'VENDOR') {
            return view('vendor.vendor-events', ['user_id' => $user->id]);
        } else {
            return redirect('home/');
        }
        
    }

    public function showEventDetail($id)
    {
        $user = Auth::user();
        if($user->type == 'VENDOR') {
            $event = Event::select('event.*', 'venue.name as venue')
                ->where('event.id_vendor', '=', $user->id)
                ->where('event.id', '=', $id)
                ->join('venue', 'event.id_venue', '=', 'venue.id')
                ->first();
        $avail = Ticket::where([
            ['id_event', '=', $id],
            ['sold', '=', false]
        ])
            ->count();
        $sold = Ticket::where([
            ['id_event', '=', $id],
            ['sold', '=', true]
        ])
            ->count();

        $tickets = Ticket::select('ticket.id as id', 'ticket.seat as name', 'ticket.price as price', 'ticket.sold as isSold')
            ->join('event', 'ticket.id_event', '=', 'event.id')
            ->where('event.id', '=', $id)
            ->get();

            return view('vendor.event-detail', ['event' => $event, 'available' => $avail, 'sold' => $sold, 'tickets' => $tickets]);
        } else {
            return redirect('home/');
        }
    }

    public function retrieveEvents($vendorId) {
        $user = Auth::user();
        if($user->type == 'VENDOR' && $user->id == $vendorId) {
            $records = Event::select('event.name as name', 'event.date as date', 'venue.name as venue', 'city.name as city', 'event.id as id')
                ->join('venue', 'event.id_venue', '=', 'venue.id')
                ->join('city', 'venue.id_city', '=', 'city.id')
                ->where('id_vendor', $vendorId)
                ->get();
            return view('includes.any.vendor-events-table', ['records' => $records]);
        } else {
            return redirect('home/');
        }
    }

    public function createEvent(Request $request) {
        $user = Auth::user();
        if($user->type == 'VENDOR') {
            $request->validate([
                'vendor_id' => 'bail|required',
                'name' => 'bail|required',
                'date' => 'bail|required',
                'category_id' => 'bail|required',
                'venue_id' => 'bail|required',
                'artists' => 'bail|required',
                'tickets' => 'bail|required',
            ]);

            // Transaction Begin
            DB::beginTransaction();

            // Create Event
            $newEvent = new Event;

            $newEvent->name = $request->input('name');
            $newEvent->date = $request->input('date');
            $newEvent->id_venue = $request->input('venue_id');
            $newEvent->id_category = $request->input('category_id');
            $newEvent->id_vendor = $request->input('vendor_id');

            $newEvent->save();

            // Create participations
            $artists = $request->input('artists');
            foreach($artists as $name) {
                // Search for artist name in DB, if not create new artist
                $artist = Artist::where('name', $name)->first();
                if($artist == null) {
                    $newArtist = new Artist;
                    $newArtist->name = $name;
                    $newArtist->save();
                    $artist = $newArtist;
                }
                // save a participation
                $newParticipation = new Participation;
                $newParticipation->id_event = $newEvent->id;
                $newParticipation->id_artist = $artist->id;
                $newParticipation->save();
            }

            // Create tickets
            $tickets = $request->input('tickets');
            foreach($tickets as $ticket) {
                $newTicket = new Ticket;
                $newTicket->sector = $ticket['group'];
                $newTicket->seat = $ticket['name'];
                $newTicket->price = $ticket['price'];
                $newTicket->sold = false;
                $newTicket->id_event = $newEvent->id;
                $newTicket->save();
            }

            // Transaction End
            DB::commit();
            
        } else {
            return redirect('home/');
        }
    }
    
    public function createVenue(Request $request) {
        $user = Auth::user();
        if($user->type == 'VENDOR') {
            $record = new Venue;

            $record->name = $request->input('name');
            $record->id_city = $request->input('city_id');
            $record->longitude = $request->input('longitude');
            $record->latitude = $request->input('latitude');

            $record->save();
        } else {
            return redirect('home/');
        }
    }

    public function deleteTicket($id) {
        $user = Auth::user();
        $ticket = Ticket::select('ticket.*', 'event.id_vendor as vendor')
        ->where('ticket.id', '=', $id)
        ->join('event', 'ticket.id_event', '=', 'event.id')
        ->first();
        if($user->type == 'VENDOR' && $user->id == $ticket->vendor) {
            $ticket->delete();
            return("Ok");
        } else {
            return("Error");
        }
    }

    public function deleteEvent($id) {
        $user = Auth::user();
        $event = Event::select()
        ->where('event.id', '=', $id)
        ->first();
        $tickets = Ticket::select()
        ->where('ticket.id_event', '=', $id)
        ->get();
        $parts = Participation::select()
        ->where('participation.id_event', '=', $id)
        ->get();
        if($user->type == 'VENDOR' && $user->id == $event->id_vendor) {
            foreach($parts as $part) {
                $part->delete();
            }
            foreach($tickets as $ticket) {
                $ticket->delete();
            }
            $event->delete();
            return("Ok");
        } else {
            return("Error");
        }
    }

}
