<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Authentication Middleware
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
        $user = Auth::user();
        if($user->type == 'USER') {
            return view('account.user-account', ['userName' => $user->name]);
        } else if($user->type == 'VENDOR') {
            return view('account.vendor-account', ['userName' => $user->name]);
        } else if($user->type == 'ADMIN') {
            return view('account.admin-account', ['userName' => $user->name]);
        } else {
            return redirect('home/');
        }
        
    }

}
