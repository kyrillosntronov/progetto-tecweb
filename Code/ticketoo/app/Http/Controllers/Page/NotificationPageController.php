<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Notification;

class NotificationPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function show()
    {
        return view('notification.notification');
    }
    
    public function delete($notification_id){
        $notification = Notification::find($notification_id);
        $notification->delete();
        return redirect('/notification');
    }

}