<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Sale;
use App\Ticket;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Event;
use Mockery\Matcher\Not;

class ShoppingCartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        return view('shoppingcart.shoppingcart', ['cartItems' => auth()->user()->cartTickets()->get()]);
    }

    public function buyItems()
    {
        $user = auth()->user();
        $user->cartTickets()->update(['sold' => true]);
        $notification = new Notification();
        $notification->id_user = Auth::id();
        $notification->title = "Order done in date ".now()->isoFormat('ll');
        $notification->message ="Thank you for buying from us! \n You ordered these items:\n";
        $events = $user->cartTickets()->get()->map(function ($ticket) {
            return $ticket->event()->first();
        });
        $events = $events->unique();
        foreach ($user->cartTickets()->get() as $ticket)
        {
            $sale = new Sale();
            $sale->id_ticket = $ticket->id;
            $sale->id_user = $user->id;
            $sale->save();
            $notification->message = $notification->message.$ticket->event()->first()->name."-".$ticket->seat."\n";
        }
        $notification->message = $notification->message."You can see all your tickets in the \"My Tickets\" section.";
        $cartItems = $user->cartItems();
        $cartItems->delete();
        $notification->is_read = 0;
        $notification->save();

        foreach ($events as $event)
        {
            if($event->is_sold_out())
            {
                $notificationVendor = new Notification();
                $notificationVendor->id_user = $event->id_vendor;
                $notificationVendor->title = "Event ".$event->name." is sold out!";
                $notificationVendor->message ="The tickets for the event ".$event->name." are over.";
                $notificationVendor->save();
            }
        }

        return redirect('/ordered');
    }

    public function remove($ticket_id){
        $ticket = Ticket::find($ticket_id);
        $ticket->is_locked = 0;
        $ticket->save();
        $ticket->cartItem()->forceDelete();
        return redirect('/cart');
    }

}
