<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Event;

class AboutUsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function show()
    {
        $userName = 'Guest';
        if (Auth::check()) {
            $userName = Auth::user()->name;
        }
        return view('aboutus.aboutus', ['userName' => $userName]);
    }

}
