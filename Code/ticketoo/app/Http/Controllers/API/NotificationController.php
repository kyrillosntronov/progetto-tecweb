<?php

namespace App\Http\Controllers;

class NotificationController extends Controller
{

    public function createNotification(Request $request) {
        $user = Auth::user();
        if($user->type == 'VENDOR' || $user->type == 'USER' || $user-> type == 'ADMIN' ) {
            $request->validate([
                'notification_id' => 'bail|required',
                'user_id' => 'bail|required',
                'message' => 'bail|required',
                'is_read' => 'bail|required',
            ]);

            // Create Notification
            $newNotification = new Notification;

            $newNotification->notification_id = $request->input('notification_id');
            $newNotification->user_id = $request->input('user_id');
            $newNotification->message = $request->input('message');
            $newNotification->is_read = $request->input('is_read');

            $newNotification->save();

        } else {
            return redirect('home/');
        }
    }

    function checkNotification(Request $request){
        $user = Auth::user();

        $notification = Notification::select('notification.id as id')
                        ->whereLike('id_user', $user->id)
                        ->whereLike('is_read', false)
                        ->get();
    
        if(!empty($notification)){
            return true;
        }
        return false;
    }
}
