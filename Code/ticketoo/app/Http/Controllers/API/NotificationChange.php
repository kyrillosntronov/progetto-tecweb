<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Http\Request;

class NotificationChange extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request , $notificationId)
    {
        $notification = Notification::find($notificationId);
        $notification->is_read = true;
        $notification->save();
        return response()->json([
            'id' => '$notificationId',
            'is_read' => true
        ]);;
    }
}
