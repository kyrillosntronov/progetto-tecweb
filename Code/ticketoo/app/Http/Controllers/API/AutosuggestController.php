<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Venue;

use App\City;

use App\Event;

use App\Artist;

use App\Participation;

class AutosuggestController extends Controller
{
    // searches for name, venue name, city name, artist name
    function searchEvent(Request $request) {
        $query = $request->input('query');

        $events = Participation::select('event.id as id', 'event.name as name', 'venue.name as venueName', 'city.name as cityName')
                        ->join('event', 'participation.id_event', '=', 'event.id')
                        ->join('artist', 'participation.id_artist', '=', 'artist.id')
                        ->join('venue', 'event.id_venue', '=', 'venue.id')
                        ->join('city', 'venue.id_city', '=', 'city.id')
                        ->where('event.name', 'like', '%' . $query . '%')
                        ->orWhere('artist.name', 'like', '%' . $query . '%')
                        ->orWhere('venue.name', 'like', '%' . $query . '%')
                        ->orWhere('city.name', 'like', '%' . $query . '%')
                        ->orderBy('event.date')
                        ->get();
        $filteredEvents = [];
        foreach($events as $event) {
            $isContained = false;
            foreach($filteredEvents as $filtered) {
                if($filtered == $event) {
                    $isContained = true;
                }
            }
            if(!$isContained) {
                array_push($filteredEvents, $event);
            }
        }

        return $filteredEvents;
                        
    }

    // searches for venue, city, country
    function searchVenue(Request $request) {
        $query = $request->input('query');

        $venues = Venue::select('venue.id as id', 'venue.name as venue', 'city.name as city', 'country.name as country')
                        ->join('city', 'venue.id_city', '=', 'city.id')
                        ->join('country', 'city.id_country', '=', 'country.id')
                        ->where('venue.name', 'like', '%' . $query . '%')
                        ->orWhere('city.name', 'like', '%' . $query . '%')
                        ->orWhere('country.name', 'like', '%' . $query . '%')
                        ->get();
        return $venues;
    }

    // searches for venue, city, country
    function searchCity(Request $request) {
        $query = $request->input('query');

        $cities = City::select('city.id as id', 'city.name as city', 'country.name as country')
                        ->join('country', 'city.id_country', '=', 'country.id')
                        ->where('city.name', 'like', '%' . $query . '%')
                        ->get();
        return $cities;
    }

    // searches for artist name
    function searchArtist(Request $request) {
        $query = $request->input('query');

        $artists = Artist::select('artist.id as id', 'artist.name as name')
                        ->where('artist.name', 'like', '%' . $query . '%')
                        ->get();
        return $artists;
    }
}
