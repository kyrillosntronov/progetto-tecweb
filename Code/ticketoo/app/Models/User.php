<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Referenced table
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function boughtTickets()
    {
        return Ticket::join('sale','ticket.id', '=', 'sale.id_ticket')->
            join('user','user.id','=','sale.id_user')->
            select('ticket.*', 'sale.created_at')->
            where('id_user','=', $this->id)->orderBy('sale.created_at', 'desc');
    }

    public function cartTickets()
    {
        return $this->hasManyThrough('App\Ticket',
            'App\ShoppingCart',
            'user_id',
            'id',
            'id',
            'ticket_id');
    }

    public function cartItems()
    {
        return $this->hasMany('App\ShoppingCart','user_id');
    }

    public function notification()
    {
        return $this->hasMany('App\Notification', 'id_user')->orderBy('created_at','desc');
    }

    public function availableNotification()
    {
        return $this->notification()->where('is_read',false);
    }
}

