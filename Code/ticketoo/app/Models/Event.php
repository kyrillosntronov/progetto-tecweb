<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Participation;

class Event extends Model
{
    protected $table = 'event';

    public function venue()
    {
        return $this->belongsTo('App\Venue', 'id_venue');
    }

    public function tickets($sector = null)
    {
        if ($sector == null) {
            return $this->hasMany('App\Ticket', 'id_event');
        }else{
            return $this->hasMany('App\Ticket', 'id_event')->where('sector', $sector);
        }
    }

    public function availableTickets($sector = null)
    {
        return $this->tickets($sector)->where('is_locked',0);
    }

    public function soldTickets($sector = null)
    {
        return $this->tickets($sector)->where('sold',1);
    }

    public function is_sold_out()
    {
        return $this->soldTickets()->count() == $this->tickets()->count();
    }


    protected $casts = [
        'date' => 'datetime',
        'created_at' => 'datetime'
    ];
}
