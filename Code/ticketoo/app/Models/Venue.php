<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    protected $table = 'venue';

    public function events()
    {
        $this->hasMany('App\Event', 'id_venue');
    }
}
