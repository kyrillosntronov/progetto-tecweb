<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'ticket';

    public function event()
    {
        return $this->belongsTo('App\Event', 'id_event');
    }

    public function cartItem(){
        return $this->hasOne('App\ShoppingCart', 'ticket_id');
    }
}
