<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    protected $table = 'shopping_carts';

    public function ticket(){
        return $this->hasOne('App\Ticket','ticket_id');
    }
}
