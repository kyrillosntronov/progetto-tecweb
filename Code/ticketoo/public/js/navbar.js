$(document).ready(function () {
    initRegisterationForm();
    initLoginForm();
});

function scrollToById(id) {
    // Scroll animation
    $('html, body').animate({
            scrollTop: $('#' + id).offset().top - $('#nav-bar-mobile').height(),
        },
        300,
        'swing'
    );
}

// Registration Modal Form

function initRegisterationForm() {
    $('#register-form').on('submit', (function (e) {
        e.preventDefault();
        let url = './register';
        let token = $('input[name="_token"]').val();
        let name = $('#register-first-name').val() + " " + $('#register-last-name').val();
        let email = $('#register-email').val();
        let password = $('#register-password').val();
        let passwordConfrim = $('#register-password-confirmation').val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                '_token': token,
                'name': name,
                'email': email,
                'password': password,
                'password_confirmation': passwordConfrim,
            },
            success: () => {
                registrationSuccess();
            },
            error: (xhr, status, error) => {
                registrationFail(JSON.parse(xhr.responseText));
            }
        });
    }));
}

function registrationSuccess() {
    $('#message-header').text('Registration was successful!');
    $('#message-box').removeClass('message-box-error');
    $('#message-box').addClass('message-box-success');
    $('#message-body').text('Your registration was successful!');
    $('#message-modal').modal('toggle');

    $('#register-first-name').val('');
    $('#register-last-name').val('');
    $('#register-email').val('');
    $('#register-password').val('');
    $('#register-password-confirmation').val('');
    $('#register-modal').modal('hide');
}

function registrationFail(response) {
    $('#message-header').text('Something went wrong!');
    $('#message-box').removeClass('message-box-success');
    $('#message-box').addClass('message-box-error');
    let errors = response.errors;
    let text = [];
    Object.keys(errors).forEach(prop => {
        text.push(errors[prop].join(' '));
    });
    $('#message-body').text(text.join(' '));
    $('#message-modal').modal('toggle');
}

// Login Modal Form

function initLoginForm() {
    $('#login-form').on('submit', (function (e) {
        e.preventDefault();
        let url = './login';
        let token = $('input[name="_token"]').val();
        let email = $('#login-email').val();
        let password = $('#login-password').val();
        let remember = $('#login-remember').val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                '_token': token,
                'email': email,
                'password': password,
                'remember': remember,
            },
            success: () => {
                loginSuccess();
            },
            error: (xhr, status, error) => {
                loginFail(JSON.parse(xhr.responseText));
            }
        });
    }));
}

function loginSuccess() {
    window.location.reload(true);
}

function loginFail(response) {
    $('#message-header').text('Something went wrong!');
    $('#message-box').removeClass('message-box-success');
    $('#message-box').addClass('message-box-error');
    let errors = response.errors;
    let text = [];
    Object.keys(errors).forEach(prop => {
        text.push(errors[prop].join(' '));
    });
    $('#message-body').text(text.join(' '));
    $('#message-modal').modal('toggle');
}
