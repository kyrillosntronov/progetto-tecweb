$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

function readNotification(idNotification) {
    let url = "./notification/" + idNotification;
    $.ajax({
        url: url,
        type:'PUT',
        success: ()=>{
                $('#notification-' + idNotification).modal('toggle');
                $("#badge-" + idNotification).hide();
                $value = $('#badge-not').html() - 1;
                if($value === 0){
                    $('#badge-not').removeClass('badge-danger').addClass('badge-dark');
                }
                $('#badge-not').html($value);
        }
     });
}
