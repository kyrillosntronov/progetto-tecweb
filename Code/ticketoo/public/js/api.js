function searchEvent(query, token) {
    let url = '/api/search/event';
    return search(url, query, token);
}

function searchVenue(query, token) {
    let url = '/api/search/venue';
    return search(url, query, token);
}

function searchCity(query, token) {
    let url = '/api/search/city';
    return search(url, query, token);
}

function searchArtist(query, token) {
    let url = '/api/search/artist';
    return search(url, query, token);

}

// Call autosuggestion api
function search(url, query, token) {
    return new Promise((resolve, reject) => {
        let data = {
            _token: token,
            query: query,
        }
        $.ajax({
            url: url,
            type:'PUT',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: (res) => {
                resolve(res);
            },
            error: (err) => {
                reject(err);
            }
         });
    }); 
}

// Debounce utility
function debounce(fn, time) {
    let timer;
    return () => {
        clearTimeout(timer);
        timer = setTimeout(() => fn.apply(this, arguments), time);
    }
}