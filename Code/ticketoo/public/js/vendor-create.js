const MAX_SUGGEST_ITEMS = 5;

$(document).ready(() => {
    initAutosuggest(3);
    initButtons();
});

function initButtons() {
    $('#add-participation').click(() => {
        addParticipation();
    });
    $('#add-tickets').click(() => {
        addTickets();
    });
    $('#submit-event-button').click(() => {
        createEvent();
    });
    $('#create-venue-button').click(() => {
        createVenue();
    });
}

async function initAutosuggest(min) {
    let token = $('[name="_token"]').val();
    // Venue
    $('#venue').keyup(debounce(() => {
        let val = $('#venue').val();
        if(val.length >= min) {
            suggestVenue(val, token);
        } else {
            clearSuggestions();
        }
    }, 500));
    $('#venue').focusout(() => {
        setTimeout(() => {
            clearSuggestions();
        }, 100)
    });
    // City
    $('#new-venue-city').keyup(debounce(() => {
        let val = $('#new-venue-city').val();
        if(val.length >= min) {
            suggestCity(val, token);
        } else {
            clearSuggestions();
        }
    }, 500));
    $('#new-venue-city').focusout(() => {
        setTimeout(() => {
            clearSuggestions();
        }, 100)
    });
    // Artist
    $('#artist').keyup(debounce(() => {
        let val = $('#artist').val();
        if(val.length >= min) {
            suggestArtist(val, token);
        } else {
            clearSuggestions();
        }
    }, 500));
    $('#artist').focusout(() => {
        setTimeout(() => {
            clearSuggestions();
        }, 100)
    });
}

async function suggestVenue(query, token) {
    // from api.js
    let data = await searchVenue(query, token);

    clearSuggestions();

    for(let i = 0; i < data.length && i < MAX_SUGGEST_ITEMS; i++) {
        let elem = data[i];
        let id = elem.id;
        let text = elem.venue + ", " + elem.city + ", " + elem.country;

        $('#suggest-venue-list').append("<li id='" + id + "' class='list-group-item suggestion'>" + text + "</li>");
        $('#' + id).click(() => selectSuggestionVenue(id));
    }
}

function selectSuggestionVenue(id) {
    $('#venue').val($('#' + id).text());
    $('#venue-id').val(id);
    clearSuggestions();
}

async function suggestCity(query, token) {
    //from api.js
    let data = await searchCity(query, token);

    clearSuggestions();

    for(let i = 0; i < data.length && i < MAX_SUGGEST_ITEMS; i++) {
        let elem = data[i];
        let id = elem.id;
        let text = elem.city + ", " + elem.country;

        $('#suggest-city-list').append("<li id='" + id + "' class='list-group-item suggestion' onclick='selectSuggestionCity(" + id + ")'>" + text + "</li>");
    }
}

function selectSuggestionCity(id) {
    $('#new-venue-city').val($('#' + id).text());
    $('#new-venue-city-id').val(id);
    clearSuggestions();
}

async function suggestArtist(query, token) {
    //from api.js
    let data = await searchArtist(query, token);
    
    clearSuggestions();

    for(let i = 0; i < data.length && i < MAX_SUGGEST_ITEMS; i++) {
        let elem = data[i];
        let id = elem.id;
        let name = elem.name;
    
        $('#suggest-artist-list').append("<li id='" + id + "' class='list-group-item suggestion'>" + name + "</li>");
        $('#' + id).click(() => selectSuggestionArtist(id));
    }
}

function selectSuggestionArtist(id) {
    $('#artist').val($('#' + id).text());
    $('#artist-id').val(id);
    clearSuggestions();
}

function clearSuggestions() {
    $('#suggest-venue-list').empty();
    $('#suggest-city-list').empty();
    $('#suggest-artist-list').empty();
}

function addParticipation() {
    // adds participation to a table
    let name = $('#artist').val();
    if(name) {
        // Create table row
        let parsedName = name.replace(' ', '_');
        $('#artists-table tr:last').after('<tr id="' + parsedName + '"> <td>' + name + '</td> ' +
        '<td><button type="button" class="btn btn-danger" onclick="removeParticipation(' +  "'" + parsedName + "'" + ')">-</button><input type="hidden" name="artist" value="' + name + '"></input></td> </tr>');
    }
}

function removeParticipation(name) {
    $('#' + name).remove();
}

function createEvent() {
    let token = $('input[name="_token"]').val();
    let vendor = $('#vendor-id').val();
    let name = $('#name').val();
    let date = $('#date').val();
    let category = $('#category').val();
    let venue = $('#venue-id').val();
    let artists = extractArtists();
    let tickets = extractTickets();
    
    if(validateEventForm()) {
        let data = {
            _token: token,
            vendor_id: vendor,
            name: name,
            date: date,
            category_id: category,
            venue_id: venue,
            artists: artists,
            tickets: tickets,
        }
        let url = "/vendor/manage/create";
        $.ajax({
            url: url,
            type:'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: (res) => {
                window.location.href = '/vendor/manage/events';
            },
            error: (xhr, status, error) => {
                alert("Something went wrong! Make sure that an event by that name doesn't exist and try again.");
            }
        });
    }
}

function validateEventForm() {
    let isValid = true;
    let token = $('input[name="_token"]').val();
    if(!token) {
        isValid = false;
    }
    let vendor = $('#vendor-id').val();
    if(!vendor) {
        isValid = false;
    }
    let name = $('#name').val();
    if(!name) {
        $('#name').addClass('is-invalid');
        isValid = false;
    }
    let date = $('#date').val();
    if(!date) {
        $('#date').addClass('is-invalid');
        isValid = false;
    }
    let category = $('#category').val();
    if(!category) {
        $('#category').addClass('is-invalid');
        isValid = false;
    }
    let venue = $('#venue-id').val();
    if(!venue) {
        $('#venue').addClass('is-invalid');
        isValid = false;
    }
    let artists = extractArtists();
    if(!artists || !artists.length) {
        $('#part-err').text("Please make sure that at least one artist has been added to the event.");
        isValid = false;
    }
    let tickets = extractTickets();
    if(!tickets || !tickets.length) {
        $('#ticket-err').text("Please make sure that at least one ticket has been added to the event and that no duplicate tickets are present.");
        isValid = false;
    }
    return isValid;
}

function extractArtists() {
    let artists = [];
    $('input[name="artist"]').each((i, e) => {
        let artist = e.value;
        artists.push(artist);
    });
    return artists;
}

var ticketCounter = 0;
function addTickets() {
    // Generates tickets based on range
    let name = $('#ticket-name').val();
    let rangeVal = $('#ticket-range').val();
    let price = $('#ticket-price').val();

    if(name && price) {
        let text = rangeVal.replace(/ /g, '');
        let range = parseRange(text);
        let number = parseNumber(text);
        if(range) {
            let from = range.from;
            let to = range.to;
            let rangeStr;
            if(from > 0 || to > 0) {
                rangeStr = '(' + from + '-' + to + ')';
            } else {
                rangeStr = '';
            }
            $('#tickets-table tr:last').after('<tr id="tck-' + ticketCounter + '" name="ticket-range"> <td>' + name + rangeStr+ '</td> ' + '<td>' + (to - from + 1) + '</td> ' + '<td>' + price + '</td> ' +
            '<td><button type="button" class="btn btn-danger" onclick="removeTickets(' +  "'tck-" + ticketCounter + "'" + ')">-</button><input type="hidden" name="ticket-name" value="' + name + '">' + 
            '</input><input type="hidden" name="ticket-range-from" value="' + from + '"></input><input type="hidden" name="ticket-range-to" value="' + to + '"></input><input type="hidden" name="ticket-price" value="' + price + '"></input></td> </tr>');
            ticketCounter++;
        } else if(number) {
            let from = 1;
            let to = number;
            let rangeStr = '(1 - ' + number + ')';
            $('#tickets-table tr:last').after('<tr id="tck-' + ticketCounter + '" name="ticket-range"> <td>' + name + rangeStr+ '</td> ' + '<td>' + (to - from + 1) + '</td> ' + '<td>' + price + '</td> ' +
            '<td><button type="button" class="btn btn-danger" onclick="removeTickets(' +  "'tck-" + ticketCounter + "'" + ')">-</button><input type="hidden" name="ticket-name" value="' + name + '">' + 
            '</input><input type="hidden" name="ticket-range-from" value="' + from + '"></input><input type="hidden" name="ticket-range-to" value="' + to + '"></input><input type="hidden" name="ticket-price" value="' + price + '"></input></td> </tr>');
        } else {
            $('#ticket-err-top').text("Invalid range or number, please make sure to input range in a correct format (see ? for help) or a single positive number.");
        }
    }
}

function parseRange(text) {
    let regexFrom = /([0-9]+)-/;
    let regexTo = /-([0-9]+)/;
    let to; 
    let from;
    if(text.match(regexFrom)) {from = text.match(regexFrom)[1];};
    if(text.match(regexTo)) {to = text.match(regexTo)[1];};
    if(from && to && from < to) {
        return {
            from: from,
            to: to,
        };
    } else {
        return null;
    }
}

function parseNumber(text) {
    let regexNumber = /[0-9]+/;
    if(text.match(regexNumber)) {
        return text.match(regexNumber);
    } else {
        return null;
    }
}

function removeTickets(name) {
    $('#' + name).remove();
}

function extractTickets() {
    let tickets = [];
    $('tr[name="ticket-range"]').each((i, e) => {
        let name = $(e).find('input[name="ticket-name"]').val();
        let from = $(e).find('input[name="ticket-range-from"]').val();
        let to = $(e).find('input[name="ticket-range-to"]').val();
        let price = $(e).find('input[name="ticket-price"]').val();
        for(let i = from; i <= to; i++) {
            let ticket = {
                group: name,
                name: name + i,
                price: price,
            };
            tickets.push(ticket);
        }
    });
    return tickets;
}

function createVenue() {
    let url = "./create/venue";
    let name = $('#new-venue-name').val();
    let city = $('#new-venue-city-id').val();
    let lat = $('#new-venue-lat').val();
    let long = $('#new-venue-long').val();
    let token = $('input[name="_token"]').val();

    if(name && city) {
        
    let data = {
        _token: token,
        name: name,
        city_id: city,
        latitude: lat,
        longitude: long,
    };

    $.ajax({
        url: url,
        type:'PUT',
        data: JSON.stringify(data),
        contentType: 'application/json',
        success: (res) => {
            $('#venue-modal').modal('toggle');
        },
        error: (xhr, status, error) => {
            $('#venue-modal-err').text("A venue with this name already exists!");
        }
     });

    }
}