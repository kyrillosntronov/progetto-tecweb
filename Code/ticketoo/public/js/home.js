const MAX_SUGGEST_ITEMS = 3;

$(document).ready(() => {
    initAutosuggest(3);
    initModal();
});

function initModal() {
    $('#search-modal').on('shown.bs.modal', function () {
        $('#event-search').focus();
    })  
}

function initAutosuggest(min) {
    let token = $('[name="_token"]').val();

    $('#event-search-main').keyup(debounce(() => {
        let val =  $('#event-search-main').val();
        $('#event-search').val(val);
        if(val.length >= min) {
            suggestEvent(val, token);
            $('#search-modal').modal();
        } else {
            clearSuggestions();
        }    
    }, 550));

    $('#event-search').keyup(debounce(() => {
        let val = $('#event-search').val();
        if(val.length >= min) {
            suggestEvent(val, token);
        } else {
            clearSuggestions();
        }
    }, 500));
}

async function suggestEvent(query, token) {
    //from api.js
    let data = await searchEvent(query, token);

    clearSuggestions();

    for(let i = 0; i < data.length && i < MAX_SUGGEST_ITEMS; i++) {
        let elem = data[i];
        let id = elem.id;
        let name = elem.name;
        let venue = elem.venueName;
        let city = elem.cityName;

        $('#suggest-event-list').append("<li id='evt-" + id + "' class='list-group-item suggestion'>" + name + ', ' + venue + ', ' + city + "</li>");
        $('#evt-' + id).click(() => selectSuggestionEvent(id));
    }
}

function clearSuggestions() {
    $('#suggest-event-list').empty();
}

function selectSuggestionEvent(id) {
    window.location = "./event/" + id;
}
