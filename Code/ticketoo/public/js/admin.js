$(document).ready(() => {
    initTabs();
    loadUsersTable();
    initResponsiveTable();
});

function initResponsiveTable() {
    // Reload window to re-draw the table with new size
    $(window).on( "orientationchange", (event) => {
        location.reload();
    });
}

function initTabs() {
    // Each tab dynamically loads new table
    $('#users-tab').on('click', () => {
        loadUsersTable();
    });
    $('#events-tab').on('click', () => {
        loadEventsTable();
    });
    $('#sales-tab').on('click', () => {
        loadSalesTable();
    });
    $('#emails-tab').on('click', () => {
        loadEmailsTable();
    });
}

function initDataTableUI(tableName) {
    // Init custom data-table layout
    let elem = '#' + tableName + '-table';
    let table = $(elem).DataTable();
    $('#search-field').keyup(() => {
        table.search($('#search-field').val()).draw();
    });
    $('#page-length').change(() => {
        table.page.len($('#page-length').val()).draw();
    });
    $('#page-prev').click(() => {
        table.page('previous');
    });
    $('#page-next').click(() => {
        table.page('next');
    });
}

function loadTable(tableName) {
    switch(tableName) {
        case 'user': loadUsersTable(); 
    }
}

function loadUsersTable() {
    let url = "./admin/table/user";
    $.ajax({
        url: url,
        type:'GET',
        success: function(data) {
            $('#table-container').html($(data));
            initUsersTable();
            initDataTableUI('user');
        }
     });
}

function initUsersTable() {
    $('#user-table').DataTable({
        dom: 't',
        columnDefs: [
            {searchable: false, targets: 4},
            {orderable: false, targets: 4},
        ]
    });
}

function loadEventsTable() {
    let url = "./admin/table/event";
    $.ajax({
        url: url,
        type:'GET',
        success: function(data) {
            $('#table-container').html($(data));
            initEventsTable();
            initDataTableUI('event');
        }
     });
}

function initEventsTable() {
    $('#event-table').DataTable({
        dom: 't',
        columnDefs: [
            
        ]
    });
}

function loadSalesTable() {
    let url = "./admin/table/sale";
    $.ajax({
        url: url,
        type:'GET',
        success: function(data) {
            $('#table-container').html($(data));
            initSalesTable();
            initDataTableUI('sale');
        }
     });
}

function initSalesTable() {
    $('#sale-table').DataTable({
        dom: 't',
        columnDefs: [
            
        ]
    });
}

function checkUserDetail(id) {
    let url = "./admin/table/user/" + id;
    $.ajax({
        url: url,
        type:'GET',
        success: function(data) {
            $('#detail-id').text('Id: ' + data['record']['id']);
            $('#detail-id').data('id', data['record']['id']);
            $('#detail-email').text('Email: ' + data['record']['email']);
            $('#detail-name').text('Name: ' + data['record']['name']);
            $('#detail-type').text('Type: ' + data['record']['type']);
            $('#detail-created').text('Created: ' + data['record']['created_at']);
            $('#detail-updated').text('Last Updated: ' + data['record']['updated_at']);
            $('#detail-modal').modal('toggle');
        }
     });
}

function createUserModal() {
    let id = $('#detail-id').data('id');
    // Creates user detail modal
    if(id) {
        let url = "./admin/table/user/" + id;
        $.ajax({
            url: url,
            type:'GET',
            success: function(data) {
                $('#create-id').val(id);
                $('#create-email').val(data['record']['email']);
                $('#create-name').val(data['record']['name']);
                $('#create-type').val(data['record']['type']);
                $('#create-modal').modal('toggle');
            }
         });
    } else {
        $('#create-id').val();
        $('#create-email').val();
        $('#create-name').val();
        $('#create-type').val();
        $('#create-modal').modal('toggle');
    }
}

function saveRecord(tableName, token) {
    let url = "./admin/table/" + tableName;
    let id = $('#create-id').val();
    let pass = $('#create-password').val();
    let data = {
        'email': $('#create-email').val(),
        'name': $('#create-name').val(),
        'type': $('#create-type').val(),
        '_token': token,
    }
    if(id > 0) {
        data.id = id;
    }
    if(pass) {
        data.password = pass;
    }
    $.ajax({
        url: url,
        type:'PUT',
        data: JSON.stringify(data),
        contentType: 'application/json',
        success: function(data) {
            window.location.reload;
        }
     });
}

function deleteRecord(tableName, token) {
    let id = $('#detail-id').text().replace('Id: ', '');
    let url = "./admin/table/" + tableName + "/" + id;
    $.ajax({
        url: url,
        type:'DELETE',
        data: {"_token": token},
        success: function(data) {
            $('#detail-modal').modal('toggle');
            loadTable(tableName);
        }
     });
}