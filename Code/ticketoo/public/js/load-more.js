$(function () {
    $(".list-group-item:hidden").slice(0, 10).show();
    if ($(".list-group-item:hidden").length === 0) {
        $("#load-more-card").hide();
    }
    $("#load-more").on('click', function (e) {
        e.preventDefault();
        $(".list-group-item:hidden").slice(0, 10).slideDown();
        if ($(".list-group-item:hidden").length === 0) {
            $("#load-more-card").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});
