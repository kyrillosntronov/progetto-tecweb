$(document).ready(() => {
    loadEventsTable();
    initResponsiveTable();
});

function loadEventsTable() {
    let url = "./events/list/" + $('#user-id').val();
    $.ajax({
        url: url,
        type:'GET',
        success: function(data) {
            $('#table-container').html($(data));
            initEventsTable();
            initDataTableUI();
        }
    });
}

function initEventsTable() {
    $('#event-table').DataTable({
        dom: 't',
        columnDefs: [
            {searchable: false, targets: 3},
            {orderable: false, targets: 3},
        ]
    });
}

function initResponsiveTable() {
    // Reload window to re-draw the table with new size
    $(window).on( "orientationchange", (event) => {
        location.reload();
    });
}

function initDataTableUI(tableName) {
    // Init custom data-table layout
    let elem = '#event-table';
    let table = $(elem).DataTable();
    $('#search-field').keyup(() => {
        table.search($('#search-field').val()).draw();
    });
    $('#page-length').change(() => {
        table.page.len($('#page-length').val()).draw();
    });
    $('#page-prev').click(() => {
        table.page('previous');
    });
    $('#page-next').click(() => {
        table.page('next');
    });
}

function deleteTicket(id) {
    $.ajax({
        url: '/vendor/manage/tickets/' + id,
        type: 'delete',
        data: {
            _token: $('input[name=_token]').val(),
        },
        success: (data) => {
            if(data == 'Ok') {
                $('#btn-' + id).replaceWith('<p><b>Deleted</b></p>');   
            }
        }
    });
}

function deleteEvent(id) {
    $.ajax({
        url: '/vendor/manage/events/' + id,
        type: 'delete',
        data: {
            _token: $('input[name=_token]').val(),
        },
        success: (data) => {
            if(data == 'Ok') {
                window.location = "/vendor/manage/events";
            }
        }
    });
}