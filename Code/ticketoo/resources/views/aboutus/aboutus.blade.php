<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <title>About us</title>
    <link href="{{ asset('css/shared.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/navbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/home.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/api.js') }}"></script>
</head>
<body>

@include('includes.shared.navbar')

@include('includes.shared.modal-login')

@include('includes.shared.modal-register')

@include('includes.shared.modal-message')

@include('includes.homepage.modal-home-search')

<div class="jumbotron" style="background-color:rgb(255, 255, 255);">
  <div class="container">
    <h1 class="display-4 font-weight-bold" style="color:#EA9C43">About Us!</h1>
    <p class="lead">
        <blockquote>
        <p>
            <em>
                "Fill your life of experiences, not things. <br>
                Have stories to tell, not stuff to show."
            </em>
        </p>
    </blockquote></p>
    <p>
        <div class="card text-center">
            <div class="card-body">
                <p class="card-text">We are a group of students that are developing this web site for a university project.
                    The purpose of the project is to show that we can develop a nice site where you can buy ticket of different
                    kind of events, such as concerts, festivals... </p>
                <p class="card-text">
                    <h5>Contacts:</h5>
                    <p>Elisa Tronetti</p>
                    <p>Angelo Filaseta</p>
                    <p>Kyrillos Ntronov</p>

                </p>
                <button class="btn btn-orange" onclick="window.location.href = '/home';">Home</button>
            </div>
        </div>
    </p>
  </div>
</div>

@include('includes.shared.footer')
</body>
