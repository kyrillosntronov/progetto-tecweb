<div class="modal fade" id="info-modal" tabindex="-1" role="dialog">
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">How to create tickets?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>To quickly create a range of tickets, you may use the "range" field.</p>
            <p>The number in the range will be appended to the ticket name: for example if a ticket is named
                "Section A" and the range is 1-30
                then the generated tickets will be named "Section A1", "SectionA2", ..., "Section A30".</p>
            <p>If the seats have lettered numbers, consider creating the tickets one row at a time and insert
                the row letter in the name field:<br>
                with name "Tribune A B" and range "1-2" you will get the following output: "Tribune A B1",
                "Tribune A B2".</p>
            <p><b>If you wish to create an un-numbered ticket, you may only input the ammout of tickets of that type to create.</b></p>
        </div>
    </div>
</div>
</div>