<div class="modal fade" id="venue-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Venue</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="venue-form" method="POST" action="/vendor/manage/create/venue">
                    @csrf
                    <label for="new-venue-name">Venue Name</label>
                    <input id="new-venue-name" name="name" type="text" class="form-control" required>
                    <label for="new-venue-city">Venue City</label>
                    <input id="new-venue-city" name="city" type="text" class="form-control" required>
                    <ul id="suggest-city-list" class="list-group suggest-list"></ul>
                    <p class="annotation">Please, select one of the suggestions.</p>
                    <input id="new-venue-city-id" name="city_id" type="hidden" required>
                    <input type="hidden" id="new-venue-long" name="longitude" value="1.0000000">
                    <input type="hidden" id="new-venue-lat" name="latitude" value="1.0000000">
                    <p class="error-text mt-4" id="venue-modal-err"></p>
                </form>
            </div>
            <div class="modal-footer">
                <button id="create-venue-button" type="button" form="venue-form"
                    class="btn btn-success">Create</button>
            </div>
        </div>
    </div>
</div>