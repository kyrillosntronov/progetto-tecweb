<section class="create-section">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-center">
                    <h2>Create Event</h2>
                </div>
            </div>
            <div class="card-body">
                <form id="event-form">
                    <div class="form-group">
                        <h3>Event Details</h3>
                        <input id="vendor-id" name="vendor-id" type="hidden" value="{{ $vendor_id }}">
                        @csrf
                        <label for="name">Name</label>
                        <input id="name" type="text" class="form-control" required>
                        <label for="date">Date</label>
                        <input id="date" type="date" class="form-control" required>
                        <label for="category">Category</label>
                        <select id="category" name="id_category" class="form-control">
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                        <label for="venue">Venue</label>
                        <div class="input-group">
                            <input id="venue" type="text" class="form-control autocomplete" required>
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button" data-toggle="modal"
                                    data-target="#venue-modal" title="create a new venue">+ New</button>
                            </div>
                        </div>
                        <input id="venue-id" name="id_venue" type='hidden'>
                        <ul id="suggest-venue-list" class="list-group suggest-list"></ul>
                        <p class="annotation">Please, select one of the suggestions.</p>
                    </div>
                    <div class="form-group">
                        <h3>Featured Artists</h3>
                        <label for="artist">Add Artist/Band</label>
                        <p class="annotation">Press '+' to add an artist to the list of participants.</p>
                        <div class="input-group">
                            <input id="artist" type="text" class="form-control autocomplete">
                            <div class="input-group-append">
                                <button id="add-participation" class="btn btn-success" type="button" title="add artist">+</button>
                            </div>
                        </div>
                        <ul id="suggest-artist-list" class="list-group suggest-list"></ul>
                        <label for="artists-tabl" class="mt-3">Participating Artists or Bands<br><span id="part-err"
                                class="text-danger"></span></label>
                        <table id="artists-table" class="table">
                            <thead id="artists-table" class="thead-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-between mb-3">
                            <h3>Tickets Details</h3>
                            <button class="btn btn-info" type="button" data-toggle="modal"
                                data-target="#info-modal" title="help">?</button>
                        </div>
                        <span id="ticket-err-top" class="text-danger"></span>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Name:</span>
                            </div>
                            <input id="ticket-name" type="text" class="form-control"
                                placeholder="es. Sector A Row B">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Range:</span>
                            </div>
                            <input id="ticket-range" type="text" class="form-control"
                                placeholder="es. number: 1 or range: 5-30">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Ticket Price:</span>
                            </div>
                            <input id="ticket-price" type="number" min="0.00" max="10000.00" step="0.01"
                                class="form-control" placeholder="es. 44.99">
                            <div class="input-group-append">
                                <span class="input-group-text">EUR</span>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button id="add-tickets" class="btn btn-success" type="button">Add Tickets</button> <br>
                        </div>
                        <label for="tickets-table" class="mt-3">Tickets that will be sold<br></label>
                        <table id="tickets-table" class="table">
                            <thead id="tickets-table" class="thead-light">
                                <tr>
                                    <th scope="col">Seat name</th>
                                    <th scope="col">Quantity sold</th>
                                    <th scope="col">Price per ticket</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <span id="ticket-err" class="text-danger"></span>
                        <div class="d-flex justify-content-center create-container">
                            <button id="submit-event-button" type="button" class="btn btn-success">Create Event</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>