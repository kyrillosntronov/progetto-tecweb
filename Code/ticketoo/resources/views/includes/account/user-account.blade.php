<section class="menu-section">
    <div class="menu-container">
    <div class="card menu-card">
        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item">
                    <a class="card-link" href="/my-tickets">My Tickets</a>
                </li>
                <li class="list-group-item">
                    <a class="card-link" href="/notification">My Notifications</a>
                </li>
            </ul>
        </div>
    </div>
    </div>
</section>
