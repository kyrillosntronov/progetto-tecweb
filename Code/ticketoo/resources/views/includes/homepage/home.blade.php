<section id="search-section" class="homepage-search-section">
        <div class="search-container">
            <h1 id="header-top">We've got the ticket...</h1>
            <input id="event-search-main" type="text" placeholder="Search for Artist, Venue, City…">
            <h2 id="header-bottom">You've been looking for!</h2>
        </div>
        <div class="arrow-container d-flex justify-content-center">
            <button class="transparent-button button-arrow-down" onclick="scrollToById('discover-section')">
                <img class="icon-arrow-down" src="{{ asset('images/arrow_down_white.png') }}" alt="white arrow pointing down">
            </button>
        </div>
    </section>

    <div class="desktop-block">
    <section id="discover-section" class="homepage-discover-section">
        <header class="discover-header d-flex align-items-center justify-content-center">
            <h1>Discover</h1>
        </header>
        <div class="category-container">
            <form action="/search#search-anchor" method="post">
            @csrf
            <div class="category">
                <button type="submit" name="category" value="1">Festivals</button>
            </div>
            <div class="category">
                <button type="submit" name="category" value="2">Concerts</button>
            </div>
            <div class="category">
                <button type="submit" name="category" value="3">Club Nights</button>
            </div>
            <div class="category">
                <button type="submit" name="category" value="4">Theater</button>
            </div>
            <div class="category">
                <button type="submit" name="category" value="5">Comedy</button>
            </div>
            </form>
        </div>
    </section>

    <section id="events-section" class="homepage-events-section">
        <header class="events-header d-flex align-items-center justify-content-center">
            <h1>Coming Soon...</h1>
        </header>
        <div class="card soon-card">
            <div class="card-body">
                <ul class="list-group list-group-flush list-events">
                    @foreach ($events as $event)
                        <li class="list-item-event" onclick="window.location.href = '/event/' + {{ $event->id }}">
                            <div>
                                <span><b>{{ $event->name }}</b></span>
                            </div>
                            <div class="pull-right-mobile">
                                <time>{{  \Carbon\Carbon::parse($event->date)->format('d/m/Y') }}</time>
                            </div>
                            <div>
                                <span>@ {{ $event->venue }}</span>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
    </div>
