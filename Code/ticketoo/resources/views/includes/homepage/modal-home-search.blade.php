<div class="modal fade" id="search-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-fullscreen" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row search-bar">
                    <div class="col-2">
                        <button type="button" class="btn close-search" data-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="col-10">
                        <input id="event-search" class="form-control" type="text">
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <ul id="suggest-event-list" class="list-group suggest-list"></ul>
            </div>
        </div>
    </div>
</div>