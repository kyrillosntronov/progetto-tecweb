<section class="filter-section">
    <div class="card">
        <div class="card-header">
            <div class="d-flex justify-content-left justify-center-d">
                <h2>Filter by:</h2>
            </div>
        </div>
        <div class="card-body">
            <div class="align-center-d">
            <form action="#search-anchor" method="post">
            @csrf
            <div class="inline-d w-30">
                <label for="category">Category</label>
                <select id="category" name="category" class="form-control">
                <option value="-1">-</option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
                </select>
            </div>
            <div class="inline-d w-70 mt-4">
                <label for="venue">Venue Name</label>
                <input id="venue" name="venue" type="text" class="form-control">
            </div>
            <div class="mt-4">
                <div class="inline-d w-49">
                    <label for="name">Event Name</label>
                    <input id="name" name="name" type="text" class="form-control">
                </div>
                <div class="inline-d wi-50">
                    <label for="city">City</label>
                    <input id="city" name="city" type="text" class="form-control">
                </div>
            </div>
            <div class="w-99 mt-4">
                <label for="date-from">Period</label>
                <div class="input-group">
                    <input id="date-from" name="dateFrom" type="date" class="form-control">
                    <input id="date-to" name="dateTo" type="date" class="form-control">
                </div>
            </div>
            <div class="w-99 mt-4">
                <label for="artist">Artist</label>
                <input id="artist" name="artist" type="text" class="form-control">
            </div>
            <div class="search-btn-container">
                <button class="btn btn-secondary" type="reset" title="clear form">Clear</button>
                <button class="btn btn-info" type="submit" title="search for events">Search</button>
            </div>
            </form>
            </div>
        </div>
    </div>
    <a id="search-anchor" class="anchor"></a>
    <div class="card">
        <div class="card-body">
            <ul id="search-result" class="search-result-list">
                @forelse ($searchResults as $event)
                <li class="search-result-item" onclick="window.location='/event/{{ $event->id }}'">
                    <div class="d-flex justify-content-between">
                    <span class="result-name"><a href="/event/{{ $event->id }}">{{ $event->name }}</a></span><span class="result-category">{{ $event->category }}</span>
                    </div>
                    <br>
                    <div class="d-flex justify-content-between">
                        <span class="result-place">{{ $event->venueName }}, {{ $event->cityName }}</span><span class="result-date">{{ $event->date }}</span>
                    </div>
                    <br>
                </li>
                @empty
                <div class="row justify-content-center">
                    @if(!$isShown)
                    <p class="empty-message">Looks like no events were found for these search parameters...</p>
                    @endif
                </div>
                @endforelse
            </ul>
        </div>
    </div>
</section>