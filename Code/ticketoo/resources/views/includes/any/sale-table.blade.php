<table id="sale-table" class="datatable table table-hover table-bordered">
    <thead>
        <tr>
            <th>User</th>
            <th>Ticket</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $sale)
        <tr>
            <td>{{ $sale->id_user }}</td>
            <td>{{ $sale->id_ticket }}</td>
            <td>{{ $sale->created_at }}</td>
        </tr>
        @endforeach
    </tbody>      
</table>