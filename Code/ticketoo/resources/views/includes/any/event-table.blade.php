<table id="event-table" class="datatable table table-hover table-bordered">
    <thead>
        <tr>
            <th>Vendor ID</th>
            <th>Name</th>
            <th>Date</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $event)
        <tr>
            <td>{{ $event->id_vendor }}</td>
            <td>{{ $event->name }}</td>
            <td>{{ $event->date }}</td>
        <td><button class="btn btn-info btn-sm" onclick="window.location = '/event/{{ $event->id }}'" title="go to event page"><b>+</b></button></td>
        </tr>
        @endforeach
    </tbody>      
</table>