<table id="event-table" class="datatable table table-hover table-bordered user-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Date</th>
            <th>Venue</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $event)
        <tr>
            <td>{{ $event->name }}</td>
            <td>{{ \Carbon\Carbon::parse($event->date)->format('d/m/Y')}}</td>
            <td>{{ $event->venue }}, {{ $event->city }}</td>
        <td><button class="btn btn-info" onclick="window.location.href = '/vendor/manage/events/{{ $event->id }}'" title="manage">+</button></td>
        </tr> 
        @endforeach
    </tbody>            
</table>