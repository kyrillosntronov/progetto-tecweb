<div class="input-group add-user">
    <button class="btn btn-success" onclick="createUserModal()">+ Add User</button>
</div>
<table id="user-table" class="datatable table table-hover table-bordered user-table">
    <thead>
        <tr>
            <th style="width:30%">Email</th>
            <th class="show-desktop">Name</th>
            <th>Type</th>
            <th class="show-desktop">Created</th>
            <th style="width:10%"class="show-mobile"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $user)
        <tr>
            <td>{{ $user->email }}</td>
            <td class="show-desktop">{{ $user->name }}</td>
            <td>{{ $user->type }}</td>
            <td class="show-desktop">{{ $user->created_at }}</td>
            <td class="show-mobile"><button class="btn btn-info btn-sm" onclick="checkUserDetail({{ $user->id }})" title="see more"><b>+</b></button></td>
        </tr> 
        @endforeach
    </tbody>            
</table>

<!-- USER DETAIL MODAL -->
<div class="modal fade" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">User Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group detail-list">
                    <li id="detail-id" class="list-group-item">Id: </li>
                    <li id="detail-email" class="list-group-item">Email: </li>
                    <li id="detail-name" class="list-group-item">Name: </li>
                    <li id="detail-type" class="list-group-item">Type: </li>
                    <li id="detail-created" class="list-group-item">Created: </li>
                    <li id="detail-updated" class="list-group-item">Last Updated: </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" onclick="deleteRecord('user', '{{ csrf_token() }}')">Delete</button>
                <button class="btn btn-info" onclick="createUserModal()">Edit</button>
            </div>
        </div>
    </div>
</div>

<!-- USER CREATION MODAL -->
<div class="modal fade" id="create-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create/Update User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input id="create-id" class="form-control" type="text" placeholder="ID will be generated." disabled></input>
                        <input id="create-email" class="form-control" type="text" placeholder="Email"></input>
                        <input id="create-password" class="form-control" type="text" placeholder="Password"></input>
                        <input id="create-name" class="form-control" type="text" placeholder="Name"></input>
                        <input id="create-type" class="form-control" type="text" placeholder="Type"></input>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-success" onclick="saveRecord('user', '{{ csrf_token() }}')">Save</buton>
                    </div>
                </form
            </div>
        </div>
    </div>
</div>
