<footer class="page-footer pt-4 mt-auto">
    <div class="container-fluid text-center">
        <div class="row justify-content-center">
            <div class="col-8">
                <h5 class="text-uppercase">Links</h5>
                <div class="row">
                    <div class="col-6">
                        <a href="/home">
                            <p>Home</p>
                        </a>
                    </div>
                    <div class="col-6">
                        <a href="/about">
                            <p>About us</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="d-flex justify-content-center">
            <span class="footer-copyright">
                © 2020 Copyright Elisa Tronetti, Angelo Filaseta, Kyrillos Ntronov
            </span>
        </div>
    </div>
</footer>


