<div class="modal fade" id="login-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="login-form" method="POST" action="/login">
                    @csrf
                    <label for="login-email">Email</label>
                    <input id="login-email" class="form-control" type="email" required>
                    <label for="login-password">Password</label>
                    <input id="login-password" class="form-control" type="password" required>
                    <div class="form-check form-check-inline login-check">
                        <input id="login-remember" class="form-check-input" type="checkbox">
                        <label class="form-check-label" for="login-remember">Remember Me</label>
                    </div>
                </form>
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-orange" data-toggle="modal" data-target="#register-modal">Register</button>
                <button type="submit" form="login-form" class="btn btn-orange">Login</button>
            </div>
        </div>
    </div>
</div>