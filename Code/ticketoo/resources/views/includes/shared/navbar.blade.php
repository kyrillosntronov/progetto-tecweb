<!-- Cart not available modal -->
<div class="modal fade" id="cartNotAvailableModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cartNotAvailableLabel">Cart not available</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                You first need to login!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-orange" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<nav id="nav-bar-mobile" class="shared-nav">
    <div class="d-flex justify-content-between align-items-center">
        <a href="/home" class="mr-auto" title="Go Home">
            <img class="nav-logo nav-button-menu" src="{{ asset('images/ticketoo_logo.png') }}">
        </a>
        @auth
            <div class="d-flex flex-row">
            <button class="transparent-button" onclick="window.location.href='/notification'">
                <img class = "nav-icon-bell" src="{{ asset('images/bell.png') }}" alt="white bell icon">
                @if(Auth::user()->availableNotification()->count() > 0)
                    <span id="badge-not" class="badge badge-danger">{{ Auth::user()->availableNotification()->count()}}</span>
                @else
                    <span id="badge-not" class="badge badge-dark">0</span>
                @endif
            </button>
            </div>
            <button class="transparent-button" onclick="window.location.href='/cart'">
                <img class = "nav-icon-cart" src="{{ asset('images/shopping_cart.png') }}" alt="white shopping cart icon">
                @if(Auth::user()->cartTickets()->count() > 0)
                    <span class="badge badge-danger">{{Auth::user()->cartTickets()->count()}}</span>
                @else
                    <span class="badge badge-dark">0</span>
                @endif
            </button>
        @endauth
        @guest
            <button class="transparent-button" data-toggle="modal" data-target="#cartNotAvailableModal">
                <img class = "nav-icon-cart" src="{{ asset('images/shopping_cart.png') }}" alt="white shopping cart icon">
            </button>
        @endguest
        <button class="navbar-toggler transparent-button nav-button-menu" data-toggle="collapse" data-target="#menu-content">
            <img class = "nav-icon-menu" src="{{ asset('images/menu.png') }}" alt="white menu icon">
        </button>
    </div>

    <!-- Hamburger menu -->
    <div class="nav-menu collapse navbar-collapse" id="menu-content">
        <ul class="navbar-nav mr-auto">
            @auth
                <li class="nav-item">
                    <p class="menu-greet">Hello, {{ auth()->check() ? auth()->user()->name : 'Guest' }}!</p>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/logout"><u>Logout</u></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/account"><u>Account</u></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/my-tickets"><u>My Tickets</u></a>
                </li>
            @endauth
            @guest
            <li class="nav-item">
                <a class="nav-link menu-link" data-toggle="modal" href="#login-modal"><u>Login/Register</u></a>
            </li>
            @endguest
            <li class="nav-item">
                <a class="nav-link  menu-link" href="/search"><u>Search for tickets</u></a>
            </li>
            <li class="nav-item">
                <a class="nav-link menu-link" href="/about"><u>About us</u></a>
            </li>
        </ul>
  </div>
</nav>
