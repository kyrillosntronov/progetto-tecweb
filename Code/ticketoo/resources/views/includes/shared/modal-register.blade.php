<div class="modal fade" id="register-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Register</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form id="register-form" method="POST" action="/register">
                @csrf
                <label for="register-first-name">First Name</label>
                <input id="register-first-name" class="form-control" type="text">
                <label for="register-last-name">Last Name</label>
                <input id="register-last-name" class="form-control" type="text">
                <label for="register-email">Email</label>
                <input id="register-email" class="form-control" type="email">
                <label for="register-password">Password</label>
                <input id="register-password" class="form-control" type="password" placeholder="At least 8 characters">
                <label for="register-password">Password Confirmation</label>
                <input id="register-password-confirmation" class="form-control" type="password" placeholder="Must match the above password">
            </form>
            </div>
             <div class="modal-footer">
                 <button type="submit" form="register-form" class="btn btn-orange">Register</button>
            </div>
        </div>
    </div>
</div>