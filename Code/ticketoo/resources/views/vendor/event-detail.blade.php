<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <title>Ticketoo - Buy tickets for events online</title>
    <link href="{{ asset('css/shared.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor-events.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor-events.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body>

    @include('includes.shared.navbar')

    <section class="events-section">
        <div class='event-header'>
            <h1>{{ $event->name }}</h1>
            <h2>{{ $event->date}}</h2>
            <h3>@ {{$event->venue}}</h2>
        </div>
    </section>
    <section class="dash-section">
        <div class="d-flex justify-content-center">
            <button class="btn btn-danger delete-btn" onclick="deleteEvent({{ $event->id }})">Delete Event</button>
        </div>
        <div class="event-dash">
            <div class="container-fluid mt-4">
                <div class="row">
                    <div class="col dash-counter">
                        <h5 id="available"><b>{{ $available }}</b></h5>
                            <label for="available">Available</label>
                        </div>
                    <div class="col dash-counter">
                        <h5 id="sold"><b>{{ $sold }}</b></h5>
                        <label for="sold">Sold</label>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="table-section">
        {{ csrf_field() }}
        <table id="ticket-table" class="datatable table table-hover table-bordered">
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Sold</th>
                <th></th>
            </tr>
            @foreach ($tickets as $ticket)
            <tr>
                <td>{{ $ticket->name }} </td>
                <td>{{ $ticket->price }}</td>
                <td>
                    @if ( $ticket->isSold )
                        <p id="tck-{{ $ticket->id }}"><b>Yes</b></p>
                    @else
                        <p id="tck-{{ $ticket->id }}"><b></b></p>
                    @endIf
                </td> 
                <td style="text-align:center">
                    <button id="btn-{{ $ticket->id }}" title="Delete" class="btn btn-danger" onclick="deleteTicket({{ $ticket->id }})" title="delete">-</button>
                </td>  
            </tr>
            @endforeach
        </table>
    </section>

    @include('includes.shared.modal-login')

    @include('includes.shared.modal-register')

    @include('includes.shared.footer')

</body>