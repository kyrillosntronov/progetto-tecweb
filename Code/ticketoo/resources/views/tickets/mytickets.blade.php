<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <link href="{{ asset('css/shared.css') }}" rel="stylesheet">
    <link href="{{ asset('css/event.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/navbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/load-more.js') }}"></script>
    <title>Ticketoo - Shopping Cart</title>
</head>

<body>

@include('includes.shared.navbar')

@if(auth()->user()->boughtTickets()->count() > 0)
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 px-1">
                <div class="card">
                    <div class="card-header">
                        <h1>Your Tickets</h1>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($tickets as $ticket)
                                @component('tickets.modal-ticket-info')
                                        @slot('eventName')
                                             {{$ticket->event()->first()->name}}
                                        @endslot
                                        @slot('sector')
                                            {{$ticket->sector}}
                                        @endslot

                                        @slot('seat')
                                            {{$ticket->seat}}
                                        @endslot
                                        @slot('price')
                                            {{$ticket->price}}
                                        @endslot
                                        @slot('date')
                                            {{$ticket->event()->first()->date->isoFormat('ll')}}
                                        @endslot
                                    {{$ticket->id}}
                                @endcomponent
                                <li style="display: none" class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <h4>{{$ticket->event()->first()->name}}</h4>
                                        <div>
                                        <h5><b>Price:</b> {{$ticket->price}}€</h5>
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-ticket-{{ $ticket->id }}">
                                            Info
                                        </button>
                                        </div>
                                    </div>
                                    <h5><b>Seat:</b> {{$ticket->seat}}</h5>
                                    <h5><b>Bought on </b> {{$ticket->created_at->isoFormat('ll')}}</h5>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="load-more-card" class="card text-center">
        <div class="card-header">
            You can see your older ticket by clicking "Load more".
        </div>
        <div class="card-body">
            <button class="btn btn-orange" id="load-more">
                Load More
            </button>
        </div>
    </div>
@else
    <div class="card">
        <div class="card-header">
            Go Shopping!
        </div>
        <div class="card-body">
            <h5 class="card-title">You haven't bought anything yet!</h5>
            <p class="card-text">You can search for events and buy tickets from us! Just search an event.</p>
            <button class="btn btn-orange" onclick="window.location.href = '/home';">Back to Home</button>
        </div>
    </div>
@endif

@include('includes.shared.footer')

</body>
</html>

