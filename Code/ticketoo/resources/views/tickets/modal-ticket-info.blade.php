<div class="modal fade" id="modal-ticket-{{$slot}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="ticketGroup modal-header">
                <h5 class=" modal-title">Ticket Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>{{$eventName}}</h3>
                <hr>
                <h4><b>Sector:</b> {{$sector}}</h4>
                <h4><b>Seat:</b> {{$seat}}</h4>
                <hr>
                <h5>The price of the Ticket was <b>{{$price}}€</b></h5>
                <p>The event will be <b>{{$date}}</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-orange" data-dismiss="modal">Go Back</button>
            </div>
        </div>
    </div>
</div>
