<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <title>Ticketoo - Buy tickets for events online</title>
    <link href="{{ asset('css/shared.css') }}" rel="stylesheet">
    <link href="{{ asset('css/homepage.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/navbar.js') }}"></script>
    <script src="{{ asset('js/home.js') }}"></script>
    <script src="{{ asset('js/api.js') }}"></script>
</head>
<body>

    @include('includes.shared.navbar')

    @include('includes.homepage.home')

    @include('includes.shared.modal-login')

    @include('includes.shared.modal-register')

    @include('includes.shared.modal-message')

    @include('includes.homepage.modal-home-search')

    @include('includes.shared.footer')

</body>
