<div class="modal fade" id="modal-sector-{{strtolower(str_replace(' ', '', $slot))}}"" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="ticketGroup modal-header">
                <h5 class=" modal-title">Tickets for <b>{{App\Event::find($eventID)->name}}</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($availableTickets->toHtml() <= 5)
                    <h5>Available Tickets:  <span class="text-danger">{{$availableTickets}}</span></h5>
                @else
                    <h5>Available Tickets: {{$availableTickets}}</h5>
                @endif
                <hr>
                <h5>You will be placed in <b>{{$slot}}</b></h5>
                <p>Please choose the number of tickets to buy.</p>
                <div class="block">
                    <form id="add-form-{{strtolower(str_replace(' ', '', $slot))}}" method="POST" action="/addtocart">
                        @csrf
                        <div class="input-group">
                            <input name="event" type="hidden" value="{{$eventID}}">
                            <input name="sector" type="hidden" value="{{$slot}}">
                            <label>Number of seats: </label>
                            <input type="number" name="quantity" value="1" min="1" max="{{$availableTickets}}" step="1"/>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Go Back</button>
                <button type="submit" form="add-form-{{strtolower(str_replace(' ', '', $slot))}}" class="btn btn-orange">Add to Cart</button>
            </div>
        </div>
    </div>
</div>
