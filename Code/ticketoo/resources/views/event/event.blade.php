<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <link href="{{ asset('css/shared.css') }}" rel="stylesheet">
    <link href="{{ asset('css/event.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/navbar.js') }}"></script>
    <title>Ticketoo - Buy tickets for events online</title>
</head>
<body>

    @include('includes.shared.navbar')

    <section class="header-section">
        <div class="event-header">
            <div class="event-header-text row h-100 text-white text-center flex-column justify-content-center">
                <h1 class="mb-3">{{ $event->name }}</h1>
                <h2 class="mb-3">{{ $event->date->isoFormat('ll') }}</h2>
                <h3 class="mb-3">@ {{$event->venue->name}}</h3>
            </div>
        </div>
        <div class="artist-box">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <h3 class="feat">Featuring:</h3>
                </div>
            </div>
            @foreach ($artists as $artist)
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <h4 class="artist-name">{{ $artist->artist }}</h4>
                </div>
            </div>
            @endforeach
        </div>
    </section>
    @if(!$event->is_sold_out())
        <section class="ticket-section">
            <div class="event-dash">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-4 dash-counter">
                        <h5 id="available"><b>{{ $available }}</b></h5>
                            <label for="available">Available</label>
                        </div>
                        <div class="col-4 dash-counter">
                            <h5 id="booked"><b>{{ $booked }}</b></h5>
                            <label for="sold">Booked</label>
                        </div>
                        <div class="col-4 dash-counter">
                            <h5 id="sold"><b>{{ $sold }}</b></h5>
                            <label for="sold">Sold</label>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="table-section">
            @auth
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="sector-h">Sector</th>
                        <th class="tickets-h">Available Tickets</th>
                        <th class="price-h">Price</th>
                        <th class="button-h"></th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($ticketGroups as $group)
                    @component('event.modal-add-cart')
                        @slot('eventID')
                            {{$event->id}}
                        @endslot
                        @slot('availableTickets')
                            {{$event->availableTickets($group->sector)->count()}}
                        @endslot
                        {{$group->sector}}
                    @endcomponent
                    <tr>
                        <th scope="row">{{ $group->sector }}</th>
                        @if($event->availableTickets($group->sector)->count() <= 5)
                            <td class="text-danger"><b>{{$event->availableTickets($group->sector)->count()}}</b></td>
                        @else
                            <td>{{$event->availableTickets($group->sector)->count()}}</td>
                        @endif
                        <td>{{ $group->price }}€</td>
                        <td>
                            @if($event->availableTickets($group->sector)->count() > 0)
                                <button type="button" class="btn btn-orange" data-toggle="modal" data-target="#modal-sector-{{strtolower(str_replace(' ', '', $group->sector))}}" title="add to cart">
                                    +
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                </table>
                @endauth
                @guest
                <div class="row justify-content-center m-3">
                    <div class="col-11 col-md-8">
                        <div class="card card-body text-center">
                            <h5>If you want to buy some tickets, you first need to Login!</h5>
                        </div>
                    </div>
                </div>
                @endguest
        </section>

    @else

        <div class="card text-center">
            <div class="card-header">
                <h1 class="text-danger"> Event is SOLD OUT </h1>
            </div>
            <div class="card-body">
                <h5 class="card-title">Tickets are no longer available!</h5>
                <p class="card-text">However, you can still search for other events.</p>
                <button class="btn btn-orange" onclick="window.location.href = '/home';"> Go Back </button>
            </div>
        </div>

    @endif

    @include('includes.shared.modal-login')

    @include('includes.shared.modal-register')

    @include('includes.shared.modal-message')

    @include('includes.shared.footer')

</body>

