<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <link href="{{ asset('css/shared.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/navbar.js') }}"></script>
    <title>Order Successful</title>
</head>

<body>

@include('includes.shared.navbar')

@include('includes.shared.modal-login')

@include('includes.shared.modal-register')

<div class="jumbotron">
    <h1 class="display-4">We Received Your Order!</h1>
    <p class="lead">Thank you for buying from us! You will receive your items as soon as possible!</p>
    <hr class="my-4">
    <p class="lead">
        <button class="btn btn-orange" onclick="window.location.href = '/home';">Back to Home</button>
    </p>
</div>

@include('includes.shared.footer')

</body>
</html>
