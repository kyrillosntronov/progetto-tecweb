<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <link href="{{ asset('css/shared.css') }}" rel="stylesheet">
    <link href="{{ asset('css/event.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/navbar.js') }}"></script>
    <title>Ticketoo - Shopping Cart</title>
</head>

<body>

@include('includes.shared.navbar')

@if(auth()->user()->cartTickets()->count() > 0)
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 px-1">
                <div class="card">
                    <div class="card-header">
                        <h1>Your Cart</h1>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($cartItems as $item)
                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <h4>{{$item->event()->first()->name}}</h4>
                                        <h5>{{ $item->price }}€</h5>
                                        <form action="/cart/delete/{{$item->id}}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-danger">
                                                -
                                            </button>
                                        </form>
                                    </div>
                                    <h6><b>{{ $item->seat }}</b></h6>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card text-center">
        <div class="card-header">
            Grand Total
        </div>
        <div class="card-body">
            <h5 class="card-title">{{auth()->user()->cartTickets()->sum('price')}} €</h5>
            <p class="card-text">You order is ready!</p>
            <form id="buyForm" method="POST" action="/buy-items">
                @csrf
                <button type="submit" form="buyForm" class="btn btn-orange">
                    Buy all the tickets!
                </button>
            </form>
        </div>
        <div class="card-footer text-muted">
            Items in your cart: {{auth()->user()->cartTickets()->count()}}
        </div>
    </div>
@else
    <div class="card">
        <div class="card-header">
            Go Shopping!
        </div>
        <div class="card-body">
            <h5 class="card-title">Your Cart is empty!</h5>
            <p class="card-text">You can add some ticket to you Cart and come back later.</p>
            <button class="btn btn-orange" onclick="window.location.href = '/home';">Click Here</button>
        </div>
    </div>
@endif

@include('includes.shared.footer')

</body>
</html>

