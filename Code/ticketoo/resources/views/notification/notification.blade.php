<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Notification</title>
    <link href="{{ asset('css/shared.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/navbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/home.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/api.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/notification.js') }}"></script>
</head>
<body>

@include('includes.shared.navbar')

@include('includes.shared.modal-login')

@include('includes.shared.modal-register')

@include('includes.shared.modal-message')

@include('includes.homepage.modal-home-search')

@auth
    @if(auth()->user()->notification()->count() > 0)
        @foreach(auth()->user()->notification()->get() as $not)
            @component('notification.modal-read-notification')
                @slot('notificationId')
                    {{$not->id}}
                @endslot
                @slot('notificationTitle')
                    {{$not->title}}
                @endslot
                @slot('notificationTime')
                    {{$not->created_at}}
                @endslot
                {{$not->message}}
            @endcomponent
            <div class="card">
                <div class="card-header">
                    @if($not->is_read == 0)
                        <span id="badge-{{$not->id}}" class="badge badge-danger">New!</span>
                    @endif
                    {{$not->created_at}}
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{$not->title}}</h5>
                    <p style="white-space: pre-line" class="card-text">{{ Str::limit($not->message, 55) }}</p>
                </div>
                <div class="modal-footer">
                    <form method="POST" action="notification/{{$not->id}}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger">
                            <img class = "trash_bin" src="{{ asset('images/trash_bin.png') }}" alt="trash bin icon">
                        </button>
                    </form>
                    <form method="POST" action="notification/{{$not->id}}">
                        @method('PUT')
                        @csrf
                        <button type="button" class="btn btn-orange" onClick="readNotification({{$not->id}})">Open</button>
                    </form>
                </div>
            </div>
        @endforeach
    @else
        <div class="card">
            <div class="card-header">
                No notifications!
            </div>
            <div class="card-body">
                <h5 class="card-title">You don't have any notification.</h5>
                <p class="card-text">Come back to check your notifications when you have some.</p>
                <button class="btn btn-orange" onclick="window.location.href = '/home';">Home</button>
            </div>
        </div>

    @endif
@endauth
@include('includes.shared.footer')

</body>
