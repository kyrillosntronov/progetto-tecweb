<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <title>Ticketoo - Buy tickets for events online</title>
    <link href="{{ asset('css/shared.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>

<body>
    <section id="admin-console" class="show-mobile admin-console-section">
        <div class="card text-center">
            <div class="card-header">
                <h1 class="card-title">Admin Dashboard</h1>
                <ul class="nav nav-tabs card-header-tabs nav-justified">
                    <li class="nav-item">
                        <a id="users-tab" class="nav-link active" data-toggle="tab" href="#">Users</a>
                    </li>
                    <li class="nav-item">
                        <a id="events-tab" class="nav-link" data-toggle="tab" href="#">Events</a>
                    </li>
                    <li class="nav-item">
                        <a id="sales-tab" class="nav-link" data-toggle="tab" href="#">Sales</a>
                    </li>
                </ul>
            </div>
            <div class="card-body admin-body">
                <div class="form-group">
                    <input id="search-field" class="form-control" type="text" placeholder="search..."></input>
                </div>
                <div class="d-flex justify-content-center">
                    <button id="page-prev" class="btn btn-info">
                        <</button> <select id="page-length" class="custom-select">
                            <option selected value="10">10</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="-1">All</option>
                            </select>
                            <button id="page-next" class="btn btn-info">></button>
                </div>
            </div>
            <div id="table-container" class="card-body admin-body">

            </div>
        </div>
    </section>
</body>