<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->date('date');
            $table->unsignedBigInteger('id_venue');
            $table->unsignedBigInteger('id_category');
            $table->unsignedBigInteger('id_vendor');
            $table->timestamps();

            $table->foreign('id_venue')->references('id')->on('venue');
            $table->foreign('id_category')->references('id')->on('category');
            $table->foreign('id_vendor')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
