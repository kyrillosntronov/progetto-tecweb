<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVenue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venue', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_city');
            $table->string('name', 225)->unique();
            $table->decimal('longitude', 8, 2);
            $table->decimal('latitude', 8, 2);
            $table->timestamps();

            $table->foreign('id_city')->references('id')->on('city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venue');
    }
}
