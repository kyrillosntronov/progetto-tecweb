-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2020 at 04:45 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticketoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Eros Ramazotti', NULL, NULL),
(2, 'Romina', NULL, NULL),
(5, 'Albano', '2019-12-09 15:19:31', '2019-12-09 15:19:31'),
(6, 'Elisa Tronchetti', '2019-12-12 08:24:43', '2019-12-12 08:24:43'),
(7, 'Orlando Furioso', '2019-12-12 08:24:43', '2019-12-12 08:24:43'),
(8, 'Alessandro Talmi', '2019-12-12 11:23:19', '2019-12-12 11:23:19'),
(9, 'DJ Capo D\'Anno', '2019-12-23 18:09:00', '2019-12-23 18:09:00'),
(10, 'Rocky', '2020-01-20 13:26:09', '2020-01-20 13:26:09'),
(11, 'AngeloX', '2020-01-20 18:40:52', '2020-01-20 18:40:52');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Festival', NULL, NULL),
(2, 'Concerts', NULL, NULL),
(3, 'Club Night', NULL, NULL),
(4, 'Theater', NULL, NULL),
(5, 'Comedy', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_country` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `id_country`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Rimini', NULL, NULL),
(2, 1, 'Bologna', NULL, NULL),
(3, 1, 'Cesena', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Italy', 'IT', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `content` blob NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `id_venue` bigint(20) UNSIGNED NOT NULL,
  `id_category` bigint(20) UNSIGNED NOT NULL,
  `id_vendor` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `name`, `date`, `id_venue`, `id_category`, `id_vendor`, `created_at`, `updated_at`) VALUES
(11, 'Alexanderr', '2020-02-12', 2, 5, 2, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(12, 'Eventazzo', '2020-02-05', 1, 4, 4, '2020-01-20 18:40:52', '2020-01-20 18:40:52');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_11_17_000000_create_user', 1),
(2, '2019_11_17_000001_create_password_resets_table', 1),
(3, '2019_11_17_000002_create_failed_jobs_table', 1),
(4, '2019_11_17_000003_create_email', 1),
(5, '2019_11_17_000004_create_notification', 1),
(6, '2019_11_17_000005_create_country', 1),
(7, '2019_11_17_000006_create_city', 1),
(8, '2019_11_17_000007_create_venue', 1),
(9, '2019_11_17_000008_create_category', 1),
(10, '2019_11_17_000009_create_event', 1),
(11, '2019_11_17_000010_create_artist', 1),
(12, '2019_11_17_000011_create_participation', 1),
(13, '2019_11_17_000012_create_ticket', 1),
(14, '2019_11_17_000013_create_sale', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `participation`
--

CREATE TABLE `participation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_event` bigint(20) UNSIGNED NOT NULL,
  `id_artist` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participation`
--

INSERT INTO `participation` (`id`, `id_event`, `id_artist`, `created_at`, `updated_at`) VALUES
(14, 11, 8, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(15, 12, 1, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(16, 12, 11, '2020-01-20 18:40:52', '2020-01-20 18:40:52');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE `sale` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `id_ticket` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_event` bigint(20) UNSIGNED NOT NULL,
  `sector` varchar(100) NOT NULL,
  `seat` varchar(100) NOT NULL,
  `price` decimal(6,2) UNSIGNED NOT NULL,
  `sold` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`id`, `id_event`, `sector`, `seat`, `price`, `sold`, `created_at`, `updated_at`) VALUES
(514, 11, 'Platea A', 'Platea A1', '44.99', 1, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(515, 11, 'Platea A', 'Platea A2', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(516, 11, 'Platea A', 'Platea A3', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(517, 11, 'Platea A', 'Platea A4', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(518, 11, 'Platea A', 'Platea A5', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(519, 11, 'Platea A', 'Platea A6', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(520, 11, 'Platea A', 'Platea A7', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(521, 11, 'Platea A', 'Platea A8', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(522, 11, 'Platea A', 'Platea A9', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(523, 11, 'Platea A', 'Platea A10', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(524, 11, 'Platea A', 'Platea A11', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(525, 11, 'Platea A', 'Platea A12', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(526, 11, 'Platea A', 'Platea A13', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(527, 11, 'Platea A', 'Platea A14', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(528, 11, 'Platea A', 'Platea A15', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(529, 11, 'Platea A', 'Platea A16', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(530, 11, 'Platea A', 'Platea A17', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(531, 11, 'Platea A', 'Platea A18', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(532, 11, 'Platea A', 'Platea A19', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(533, 11, 'Platea A', 'Platea A20', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(534, 11, 'Platea A', 'Platea A21', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(535, 11, 'Platea A', 'Platea A22', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(536, 11, 'Platea A', 'Platea A23', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(537, 11, 'Platea A', 'Platea A24', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(538, 11, 'Platea A', 'Platea A25', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(539, 11, 'Platea A', 'Platea A26', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(540, 11, 'Platea A', 'Platea A27', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(541, 11, 'Platea A', 'Platea A28', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(542, 11, 'Platea A', 'Platea A29', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(543, 11, 'Platea A', 'Platea A30', '44.99', 0, '2020-01-20 14:11:57', '2020-01-20 14:11:57'),
(548, 12, 'Platea A', 'Platea A5', '59.99', 1, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(550, 12, 'Platea A', 'Platea A7', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(551, 12, 'Platea A', 'Platea A8', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(552, 12, 'Platea A', 'Platea A9', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(553, 12, 'Platea A', 'Platea A10', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(555, 12, 'Platea A', 'Platea A12', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(556, 12, 'Platea A', 'Platea A13', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(558, 12, 'Platea A', 'Platea A15', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(559, 12, 'Platea A', 'Platea A16', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(561, 12, 'Platea A', 'Platea A18', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(562, 12, 'Platea A', 'Platea A19', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(563, 12, 'Platea A', 'Platea A20', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(564, 12, 'Platea A', 'Platea A21', '59.99', 1, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(565, 12, 'Platea A', 'Platea A22', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(566, 12, 'Platea A', 'Platea A23', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(567, 12, 'Platea A', 'Platea A24', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(568, 12, 'Platea A', 'Platea A25', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(569, 12, 'Platea A', 'Platea A26', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(570, 12, 'Platea A', 'Platea A27', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(571, 12, 'Platea A', 'Platea A28', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(572, 12, 'Platea A', 'Platea A29', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52'),
(573, 12, 'Platea A', 'Platea A30', '59.99', 0, '2020-01-20 18:40:52', '2020-01-20 18:40:52');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `type` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `remember_token`, `type`, `created_at`, `updated_at`) VALUES
(2, 'Kyrillos Ntronov', 'ntronov.kyrillos@gmail.com', '$2y$10$cDBAqa6jiQSWoILZE4nTo.nxVOW/kn9q1fFVxheKtlTwWOHlCU.W.', 'kIQkxAhoz49315zT0ztJHpOTGslaoMp8EiTKUPSePIBJUQWkRynyRhc9GFJ6', 'VENDOR', '2019-12-08 08:16:52', '2019-12-08 08:16:52'),
(4, 'admin admin', 'admin@admin.com', '$2y$10$RfiF3RCaWU7GFeRTgeLgoOHAkAZeY/XXyBFcn4KfTCQ1Vb.2hV3kK', 'z8pjV0zOqrLHVbatyzs5DO959865BuqqfnoN2FQq2jM5rpmBZ5Ie3p3TES7p', 'VENDOR', '2020-01-20 15:43:42', '2020-01-20 15:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `venue`
--

CREATE TABLE `venue` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_city` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL,
  `longitude` decimal(8,2) NOT NULL,
  `latitude` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `venue`
--

INSERT INTO `venue` (`id`, `id_city`, `name`, `longitude`, `latitude`, `created_at`, `updated_at`) VALUES
(1, 1, 'RDS Stadium', '44.04', '12.58', '2019-12-08 17:46:14', '2019-12-08 17:46:14'),
(2, 1, 'Teatro Galli', '44.06', '12.57', '2019-12-08 17:47:28', '2019-12-08 17:47:28'),
(3, 2, 'Estragon', '44.52', '11.37', '2019-12-08 17:50:25', '2019-12-08 17:50:25'),
(6, 2, 'Fake', '0.00', '0.00', '2019-12-08 18:19:10', '2019-12-08 18:19:10'),
(7, 2, 'Disco Triste', '1.00', '1.00', '2019-12-12 08:54:35', '2019-12-12 08:54:35'),
(9, 3, 'Disco Star', '44.14', '12.25', '2019-12-12 11:21:58', '2019-12-12 11:21:58'),
(10, 1, 'Disco', '1001.00', '1000.00', '2020-01-21 11:49:45', '2020-01-21 11:49:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `artist_name_unique` (`name`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_name_unique` (`name`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `city_name_unique` (`name`),
  ADD KEY `city_id_country_foreign` (`id_country`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `country_name_unique` (`name`),
  ADD UNIQUE KEY `country_code_unique` (`code`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_id_user_foreign` (`id_user`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id_venue_foreign` (`id_venue`),
  ADD KEY `event_id_category_foreign` (`id_category`),
  ADD KEY `event_id_vendor_foreign` (`id_vendor`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_id_user_foreign` (`id_user`);

--
-- Indexes for table `participation`
--
ALTER TABLE `participation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participation_id_event_foreign` (`id_event`),
  ADD KEY `participation_id_artist_foreign` (`id_artist`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id_user_foreign` (`id_user`),
  ADD KEY `sale_id_ticket_foreign` (`id_ticket`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticket_id_event_foreign` (`id_event`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_email_unique` (`email`);

--
-- Indexes for table `venue`
--
ALTER TABLE `venue`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `venue_name_unique` (`name`),
  ADD KEY `venue_id_city_foreign` (`id_city`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `participation`
--
ALTER TABLE `participation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sale`
--
ALTER TABLE `sale`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=574;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `venue`
--
ALTER TABLE `venue`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_id_country_foreign` FOREIGN KEY (`id_country`) REFERENCES `country` (`id`);

--
-- Constraints for table `email`
--
ALTER TABLE `email`
  ADD CONSTRAINT `email_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_id_category_foreign` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `event_id_vendor_foreign` FOREIGN KEY (`id_vendor`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `event_id_venue_foreign` FOREIGN KEY (`id_venue`) REFERENCES `venue` (`id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Constraints for table `participation`
--
ALTER TABLE `participation`
  ADD CONSTRAINT `participation_id_artist_foreign` FOREIGN KEY (`id_artist`) REFERENCES `artist` (`id`),
  ADD CONSTRAINT `participation_id_event_foreign` FOREIGN KEY (`id_event`) REFERENCES `event` (`id`);

--
-- Constraints for table `sale`
--
ALTER TABLE `sale`
  ADD CONSTRAINT `sale_id_ticket_foreign` FOREIGN KEY (`id_ticket`) REFERENCES `ticket` (`id`),
  ADD CONSTRAINT `sale_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Constraints for table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_id_event_foreign` FOREIGN KEY (`id_event`) REFERENCES `event` (`id`);

--
-- Constraints for table `venue`
--
ALTER TABLE `venue`
  ADD CONSTRAINT `venue_id_city_foreign` FOREIGN KEY (`id_city`) REFERENCES `city` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
